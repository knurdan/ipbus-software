/*
  ---------------------------------------------------------------------------

  This is an extension of uHAL to directly access AXI slaves via the linux
  UIO driver. 

  This file is part of uHAL.

  uHAL is a hardware access library and programming framework
  originally developed for upgrades of the Level-1 trigger of the CMS
  experiment at CERN.

  uHAL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  uHAL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with uHAL.  If not, see <http://www.gnu.org/licenses/>.


  Andrew Rose, Imperial College, London
  email: awr01 <AT> imperial.ac.uk

  Marc Magrans de Abril, CERN
  email: marc.magrans.de.abril <AT> cern.ch

  Tom Williams, Rutherford Appleton Laboratory, Oxfordshire
  email: tom.williams <AT> cern.ch

  Dan Gastler, Boston University 
  email: dgastler <AT> bu.edu
      
  Arnaud Steen, National Taiwan University 
  email: asteen <AT> cern.ch
  ---------------------------------------------------------------------------
*/
/**
   @file
   @author Siqi Yuan / Dan Gastler / Theron Jasper Tarigo / Arnaud Steen
*/

#include <stdio.h>
#include <stdint.h>
#include <errno.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <algorithm>
#include <iomanip>
#include <boost/filesystem.hpp>
#include <boost/shared_ptr.hpp>
#include <uhal/Node.hpp>
#include <uhal/NodeTreeBuilder.hpp>
#include <pugixml.hpp>
#include "uhal/log/LogLevels.hpp"
#include "uhal/log/log_inserters.integer.hpp"
#include "uhal/log/log.hpp"

#include <uhal/ProtocolUIO.hpp>

#include <setjmp.h> //for BUS_ERROR signal handling

using namespace uioaxi;
using namespace boost::filesystem;


//Signal handling for sigbus
sigjmp_buf static env;
void static signal_handler(int sig){
  if(SIGBUS == sig){
    siglongjmp(env,sig);    
  }
}
//This macro handles the possibility of a SIG_BUS signal and property throws an exception
//The command you want to run is passed via ACESS and will be in a if{}else{} block, so
//Call it appropriately. 
// ex
//   old:
//     uint32_t readval = hw[da.device][da.word];
//   new:
//     uint32_t readval;
//     BUS_ERROR_PROTECTION(readval = hw[da.device][da.word])
#define BUS_ERROR_PROTECTION(ACCESS)					\
  if(SIGBUS == sigsetjmp(env,1)){					\
    uhal::exception::UIOBusError * e = new uhal::exception::UIOBusError(); \
    throw *e;								\
  }else{								\
    ACCESS;								\
  }

namespace uhal {  

  UIO::UIO (
	    const std::string& aId, const URI& aUri,
	    const boost::posix_time::time_duration&aTimeoutPeriod
	    ) :
    ClientInterface(aId,aUri,aTimeoutPeriod)
  { 
    // Get the filename of the address table from the connection file. Then read it through the NodeTreeBuilder
    // The NodeTreeBuilder should be able to just use the existing node tree rather than rebuild a new one
    NodeTreeBuilder & mynodetreebuilder = NodeTreeBuilder::getInstance();
    //boost::shared_ptr< Node > lNode ( mynodetreebuilder.getNodeTree ( tabfname , boost::filesystem::current_path() / "." ) );
    Node* lNode = ( mynodetreebuilder.getNodeTree ( std::string("file://")+aUri.mHostname , boost::filesystem::current_path() / "." ) );
    // Getting the IDs for only first layer nodes (nodes that contain device labels). matching names that doesn't contain a "."
    std::vector< std::string > top_node_Ids = lNode->getNodes("^[^.]+$");
    // For each device label, search for its matching device
    int size=0;
    for (std::vector<std::string>::iterator nodeId = top_node_Ids.begin(); nodeId != top_node_Ids.end(); ++nodeId) {
      // device number is the number read from the most significant 8 bits of the address
      // size should be read from /sys/class/uio*/maps/map0/size
      char uioname[128]="", sizechar[128]="", addrchar[128]=""; 
      uint32_t address1=0, address2=0;
      // get the device number out from the node
      //devnum += 1;//decodeAddress(lNode->getNode(*nodeId).getAddress()).device;
      // search through the file system to see if there is a uio that matches the name
      std::string uiopath = "/sys/class/uio/";
      std::string dvtpath = "/proc/device-tree/amba/";
      FILE *addrfile=0;
      FILE *sizefile=0; 
      // traverse through the device-tree, find subdir (x->path()) for which its name matches with node address
      for (directory_iterator x(dvtpath); x!=directory_iterator(); ++x){
	if (!is_directory(x->path())) {
	  continue;
	}
	uint32_t namesize=x->path().filename().native().size();
	std::size_t pos = x->path().filename().native().find("@");
	if( pos!=std::string::npos && pos+1+8==namesize ) {
	  address1 = std::strtoul( x->path().filename().native().substr(pos+1,8).c_str() , 0, 16);
	  if( address1==lNode->getNode(*nodeId).getAddress() ){
	    x->path();
	    break;
	  }
	  else continue;
	}
      }
      if(address1==0) log (Debug(), "Cannot find a device that matches label ", (*nodeId).c_str(), " device not opened!" );

      // Traverse through the /sys/class/uio directory
      for (directory_iterator x(uiopath); x!=directory_iterator(); ++x){
	if (!is_directory(x->path())) {
	  continue;
	}
	if (!exists(x->path()/"maps/map0/addr")) {
	  continue;
	}
	if (!exists(x->path()/"maps/map0/size")) {
	  continue;
	}
	addrfile = fopen((x->path()/"maps/map0/addr").native().c_str(),"r");
	fgets(addrchar,128,addrfile); fclose(addrfile);
	address2 = std::strtoul( addrchar, 0, 16);
	if (address1 == address2){
	  sizefile = fopen((x->path().native()+"/maps/map0/size").c_str(),"r");
	  fgets(sizechar,128,sizefile); fclose(sizefile);
	  //the size was in number of bytes, convert into number of uint32 -> to be checked with to kivanc
	  size=std::strtoul( sizechar, 0, 16);///4;  
	  strcpy(uioname,x->path().filename().native().c_str());
	  break;
	}
      }
      if (size==0) {
	log ( Debug() , "Error: Trouble loading device ",(*nodeId).c_str(), " cannot find device or size zero." );
      }
      openDevice(address1, size, uioname);
    }

  
    //Now that everything created sucessfully, we can deal with signal handling
    memset(&saBusError,0,sizeof(saBusError)); //Clear struct
    saBusError.sa_handler = signal_handler; //assign signal handler
    sigemptyset(&saBusError.sa_mask);
    sigaction(SIGBUS, &saBusError,&saBusError_old);  //install new signal handler (save the old one)
  }

  UIO::~UIO () {
    log ( Debug() , "UIO: destructor" );
    sigaction(SIGBUS,&saBusError_old,NULL); //restore the signal handler from before creation for SIGBUS
  }

  void 
  UIO::openDevice(uint32_t addr, uint32_t length, const char *name) {
    log ( Debug() , "UIO: openDevice : addr = ", Integer(addr,IntFmt<hex,fixed>()), " ; length = ", Integer(length), " ; name of device = /dev/", name);
    if( m_uiomap.find(addr)!=m_uiomap.end() ){
      uhal::exception::BadUIODevice0* lExc = new uhal::exception::BadUIODevice0();
      log (*lExc , "Address ", Integer(addr,IntFmt<hex,fixed>()) , " already used by a UIODevice -> check the address table file");
      throw *lExc;
      return;
    }

    std::ostringstream os(std::ostringstream::ate);
    os.str("");
    os << "/dev/" << name;
    uioaxi::UIOMemory_handler* uiomh = new uioaxi::UIOMemory_handler(os.str().c_str(),length);
    m_uiomap.insert( std::pair<uint32_t,uioaxi::UIOMemory_handler*>(addr,uiomh) );
    m_base_addrs.push_back( addr );
    log ( Debug(), "Mapped ", os.str().c_str(), " as device with a size of  ", Integer(length,IntFmt<hex,fixed>()), "; registered with address ", Integer(addr,IntFmt<hex,fixed>()) );

  }

  int
  UIO::checkDevice (uint32_t addr) {
    if( m_uiomap.find(addr)!=m_uiomap.end() ){
      uhal::exception::BadUIODevice1* lExc = new uhal::exception::BadUIODevice1();
      log (*lExc , "No device with address ", Integer(addr, IntFmt< hex, fixed>() ));
      throw *lExc;
      return 1;
    }
    return 0;
  }

  uint32_t 
  UIO::findBaseAddress(const uint32_t& aAddr) {
    uint32_t aMin(32);
    uint32_t baseAddr(0);
    for(auto addr: m_base_addrs){
      uint32_t count = aAddr ^ addr;
      if( count<aMin ){
	baseAddr = addr;
	aMin = count;
      }
    }//OK c'est pas beau
    return baseAddr;
  }

  ValHeader
  UIO::implementWrite (const uint32_t& aAddr, const uint32_t& aValue) {
    //let's check value when the node comes with a bit mask
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t writeval = aValue;
    m_uiomap[baseAddr]->m_data[aAddr-baseAddr] = writeval;
    return ValHeader();
  }

  ValHeader 
  UIO::implementBOT(){
    log ( Debug() , "Byte Order Transaction");
    uhal::exception::UnimplementedFunction* lExc = new uhal::exception::UnimplementedFunction();
    log (*lExc, "Function implementBOT() is not yet implemented.");
    throw *lExc;
    return ValHeader();
  }

  ValHeader 
  UIO::implementWriteBlock (const uint32_t& aAddr, const std::vector<uint32_t>& aValues, const defs::BlockReadWriteMode& aMode) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t lAddr ( aAddr-baseAddr );
    auto aHandler = m_uiomap[baseAddr];
    // let's try this ugly code:
    std::vector<uint32_t>::const_iterator ptr;
    for (ptr = aValues.begin(); ptr < aValues.end(); ptr++){
      uint32_t writeval = *ptr;
      aHandler->m_data[lAddr] = writeval;
      if ( aMode == defs::INCREMENTAL )
	lAddr ++;
    }
    // // or we can try :
    // if ( aMode == defs::INCREMENTAL )
    //   std::memcpy( &aHandler.m_data[lAddr], &aValues[0], aValues.size() );
    // else
    //   std::memcpy( &aHandler.m_data[lAddr], &aValues[0], 1 );
    return ValHeader();
  }

  ValWord<uint32_t>
  UIO::implementRead (const uint32_t& aAddr, const uint32_t& aMask) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    ValWord<uint32_t> vw(readval, aMask);
    valwords.push_back(vw);
    primeDispatch();
    return vw;
  }
    
  ValVector< uint32_t > 
  UIO::implementReadBlock ( const uint32_t& aAddr, const uint32_t& aSize, const defs::BlockReadWriteMode& aMode ) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t lAddr = aAddr-baseAddr;    
    // // a nice code : 
    // std::vector<uint32_t> read_vector;read_vector.reserve(aSize);
    // if ( aMode == defs::INCREMENTAL )
    //   std::copy( m_uiomap[baseAddr].m_data+lAddr,aSize,std::back_inserter(read_vector) );
    // else
    //   std::copy( m_uiomap[baseAddr].m_data,1,std::back_inserter(read_vector) );
    //or an uggly code: 
    std::vector<uint32_t> read_vector(aSize);
    std::vector<uint32_t>::iterator ptr;
    for (ptr = read_vector.begin(); ptr < read_vector.end(); ptr++){
      uint32_t readval = m_uiomap[baseAddr]->m_data[lAddr];
      *ptr = readval;
      if ( aMode == defs::INCREMENTAL )
    	lAddr ++;
    }
    return ValVector< uint32_t> (read_vector);
  }

  void
  UIO::primeDispatch () {
    // uhal will never call implementDispatch unless told that buffers are in
    // use (even though the buffers are not actually used and are length zero).
    // implementDispatch will only be called once after each checkBufferSpace.
    uint32_t sendcount = 0, replycount = 0, sendavail, replyavail;
    checkBufferSpace ( sendcount, replycount, sendavail, replyavail);
  }

  void
  UIO::implementDispatch (boost::shared_ptr<Buffers> aBuffers) {
    log ( Debug(), "UIO: Dispatch");
    for (unsigned int i=0; i<valwords.size(); i++)
      valwords[i].valid(true);
    valwords.clear();
  }

  ValWord<uint32_t> UIO::implementRMWbits ( const uint32_t& aAddr , const uint32_t& aANDterm , const uint32_t& aORterm ){
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    readval &= aANDterm;
    readval |= aORterm;
    m_uiomap[baseAddr]->m_data[aAddr-baseAddr] = readval;
    readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    return ValWord<uint32_t>(readval);
  }


  ValWord< uint32_t > UIO::implementRMWsum ( const uint32_t& aAddr , const int32_t& aAddend ) {
    uint32_t baseAddr = findBaseAddress(aAddr);
    uint32_t readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    readval += aAddend;
    m_uiomap[baseAddr]->m_data[aAddr-baseAddr] = readval;
    readval = m_uiomap[baseAddr]->m_data[aAddr-baseAddr];
    return ValWord<uint32_t>(readval);
  }
}
