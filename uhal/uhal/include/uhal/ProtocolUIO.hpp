/*
  ---------------------------------------------------------------------------

  This is an extension of uHAL to directly access AXI slaves via the linux
  UIO driver. 

  This file is part of uHAL.

  uHAL is a hardware access library and programming framework
  originally developed for upgrades of the Level-1 trigger of the CMS
  experiment at CERN.

  uHAL is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  uHAL is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with uHAL.  If not, see <http://www.gnu.org/licenses/>.


  Andrew Rose, Imperial College, London
  email: awr01 <AT> imperial.ac.uk

  Marc Magrans de Abril, CERN
  email: marc.magrans.de.abril <AT> cern.ch

  Tom Williams, Rutherford Appleton Laboratory, Oxfordshire
  email: tom.williams <AT> cern.ch

  Dan Gastler, Boston University 
  email: dgastler <AT> bu.edu
      
  Arnaud Steen, National Taiwan University 
  email: asteen <AT> cern.ch
      
  ---------------------------------------------------------------------------
*/
/**
   @file
   @author Siqi Yuan / Dan Gastler / Theron Jasper Tarigo / Arnaud Steen
*/

#ifndef _uiouhal_ProtocolUIO_hpp_
#define _uiouhal_ProtocolUIO_hpp_

#include <iostream>
#include <map>

#include <uhal/ClientInterface.hpp>
#include <uhal/ValMem.hpp>
#include "uhal/log/exception.hpp"

#include <boost/interprocess/file_mapping.hpp>
#include <boost/interprocess/mapped_region.hpp>
#include <signal.h> //for handling of SIG_BUS signals

namespace uioaxi {
  namespace bip = boost::interprocess ;
  class UIOMemory_handler{
  public:
    UIOMemory_handler(){;}
    UIOMemory_handler(const char* filename,int length)
    // to be checked if length is in unit of byte or a number of 32bits addresses
    {
      bip::file_mapping aFile( filename, bip::read_write );
      m_region = bip::mapped_region( aFile, bip::read_write, 0x0, length );//0x0 because no obvious reason to have any offset here
      m_data = (uint32_t *)static_cast<char*>( m_region.get_address() );
    }
    ~UIOMemory_handler(){;}
    bip::mapped_region m_region;
    uint32_t* m_data;
  };
}

namespace uhal {

  namespace exception
  {
    UHAL_DEFINE_EXCEPTION_CLASS ( UnmatchedLabel , "Exception class to handle the case where matching a label to a device failed." )
    UHAL_DEFINE_EXCEPTION_CLASS ( BadUIODevice0 , "Exception class to handle the case where uio tries to mmap more than once from the same address." )
    UHAL_DEFINE_EXCEPTION_CLASS ( BadUIODevice1 , "Exception class to handle the case where uio device cannot be opened." )
    UHAL_DEFINE_EXCEPTION_CLASS ( UnimplementedFunction , "Exception class to handle the case where an unimplemented function is called." )
    UHAL_DEFINE_EXCEPTION_CLASS ( UIOBusError , "Exception class for when an axi transaction causes a BUS_ERROR." )
  }

  class UIO : public ClientInterface {
  public:
    UIO (
	 const std::string& aId, const URI& aUri,
	 const boost::posix_time::time_duration&aTimeoutPeriod =
	 boost::posix_time::milliseconds(10)
	 );
    virtual ~UIO ();
  protected:
    ValHeader implementWrite (const uint32_t& aAddr, const uint32_t& aValue);
    ValWord<uint32_t> implementRead (const uint32_t& aAddr,const uint32_t& aMask = defs::NOMASK);
    void implementDispatch (boost::shared_ptr<Buffers> aBuffers) /*override*/ ;

    ValHeader implementBOT();
    ValHeader implementWriteBlock (const uint32_t& aAddr, const std::vector<uint32_t>& aValues, const defs::BlockReadWriteMode& aMode=defs::INCREMENTAL);
    ValVector< uint32_t > implementReadBlock ( const uint32_t& aAddr, const uint32_t& aSize, const defs::BlockReadWriteMode& aMode=defs::INCREMENTAL );
    ValWord< uint32_t > implementRMWbits ( const uint32_t& aAddr , const uint32_t& aANDterm , const uint32_t& aORterm );
    ValWord< uint32_t > implementRMWsum ( const uint32_t& aAddr , const int32_t& aAddend );
    
    /*** Unused methods from virtual inherited methods ***/
    uint32_t getMaxNumberOfBuffers() {return 0;}
    uint32_t getMaxSendSize() {return 0;}
    uint32_t getMaxReplySize() {return 0;}

    virtual  exception::exception* validate ( uint8_t* aSendBufferStart ,
					      uint8_t* aSendBufferEnd ,
					      std::deque< std::pair< uint8_t* , uint32_t > >::iterator aReplyStartIt ,
					      std::deque< std::pair< uint8_t* , uint32_t > >::iterator aReplyEndIt ) {
      return NULL;}
    /*****************************************************/
  private:
  
    std::map<uint32_t,uioaxi::UIOMemory_handler*> m_uiomap; //key : device address from address xml file
    std::vector<uint32_t> m_base_addrs; //key : device address from address xml file
  
    std::vector< ValWord<uint32_t> > valwords;
    void primeDispatch ();
    void openDevice (uint32_t addr, uint32_t length, const char *name);
    int checkDevice (uint32_t addr);
    uint32_t findBaseAddress(const uint32_t& aAddr);
    struct sigaction saBusError;
    struct sigaction saBusError_old;
  };

}
#endif
